﻿namespace MVVM.Data
{
    using System;
    using System.Collections.Generic;
    using MVVM.Models;

    public static class NinjaDataBase
    {
        public static List<Ninja> Ninjas  = new List<Ninja>
        {
            { new Ninja { Name = "Fujibayashi" } },
            { new Ninja { Name = "Hattori" } },
            { new Ninja { Name = "Ishikawa" } },
            { new Ninja { Name = "Mochizuki" } },
            { new Ninja { Name = "Momochi" } },
        };

        public static Ninja LoadNinja()
        {
            Random random = new Random();
            int position = random.Next(0, 4);
            return Ninjas[position];
        }

        public static List<Ninja> LoadNinjas()
        {
            return Ninjas;
        }
    }
}