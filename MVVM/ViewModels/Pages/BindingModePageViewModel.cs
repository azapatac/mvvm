﻿namespace MVVM.ViewModels
{
    using MVVM.Data;

    public class BindingModePageViewModel : BaseViewModel
    {
        NinjaViewModel ninja;
        public NinjaViewModel Ninja
        {
            get
            {
                return ninja;
            }
            set
            {
                if (value != ninja)
                {
                    ninja = value;
                    OnPropertyChanged();
                }
            }
        }

        public BindingModePageViewModel()
        {
            LoadNinja();
        }

        private void LoadNinja()
        {
            var _ninja = NinjaDataBase.LoadNinja();
            Ninja = new NinjaViewModel { Name = _ninja.Name };
        }
    }
}