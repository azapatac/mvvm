﻿namespace MVVM.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows.Input;
    using MVVM.Data;
    using MVVM.Views.Pages;
    using Xamarin.Forms;

    public class NinjasPageViewModel : BaseViewModel
    {
        public ICommand BindingModeCommand { get; set; }

        IList<NinjaViewModel> ninjas;
        public IList<NinjaViewModel> Ninjas
        {
            get
            {
                return ninjas;
            }
            set
            {
                if (value != ninjas)
                {
                    ninjas = value;
                    OnPropertyChanged();
                }
            }
        }

        public NinjasPageViewModel()
        {
            BindingModeCommand = new Command(BindingMode);
            Ninjas = new ObservableCollection<NinjaViewModel>();
            Title = "Ninjas Page";

            var _ninjas = NinjaDataBase.LoadNinjas();

            _ninjas.ForEach((ninja) =>
            {
                Ninjas.Add(new NinjaViewModel { Name = ninja.Name });
            });
        }

        private void BindingMode()
        {
            App.Current.MainPage.Navigation.PushAsync(new BindingModePage());
        }
    }
}