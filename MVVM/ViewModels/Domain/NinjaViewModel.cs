﻿namespace MVVM.ViewModels
{
    using System.Windows.Input;
    using Xamarin.Forms;

    public class NinjaViewModel : BaseViewModel
    {
        public ICommand ShowNameCommand { get; set; }

        string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (value != name)
                {
                    name = value;
                    OnPropertyChanged();
                }
            }
        }

        public NinjaViewModel()
        {
            ShowNameCommand = new Command(ShowName);
        }

        private void ShowName()
        {
            App.Current.MainPage.DisplayAlert(".Net University", Name, "Ok");
        }
    }
}